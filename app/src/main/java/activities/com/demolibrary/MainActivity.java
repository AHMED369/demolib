package activities.com.demolibrary;

import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Toast;

import activities.com.libdemo.MyTasks;
import activities.com.libdemo.TimeComplete;
import activities.com.libdemo.TimeDelay;
import activities.com.libdemo.ViewType;

public class MainActivity extends AppCompatActivity {
    MyTasks mt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                startDemo();
                }
        }, 2000);
        findViewById(R.id.link_to_register).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "Button Click", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        if (mt != null) {
            mt.cancel();
        }

        super.onDestroy();
    }

    private void startDemo() {
        mt = new MyTasks();
        mt.add(new TimeDelay(this, 1000, 6000, ViewType.EditText, R.id.txtuser, "xara_123", R.raw.a3, "Enter User Name"));
        mt.add(new TimeDelay(this, 1000, 3000, ViewType.EditText, R.id.txtpassword, "ABCD", R.raw.a4, "Enter Password"));
        mt.add(new TimeDelay(this, 1000, 4000, ViewType.View, R.id.btnLogin, null, R.raw.login, "Press Login Button"));
        mt.add(new TimeDelay(this, 1000, 4000, ViewType.Button, R.id.link_to_register, null, R.raw.a6, "New User"));
        /* mt.add(new TimeDelay(this, 1000, 3000, ViewType.EditText, R.id.txtuser, null, R.raw.aaa, "Now You Try"));*/


        mt.setOnCompleteTasksListner(new TimeComplete() {
            @Override
            public void onTimeComplete() {
               /* editTextUserName.getText().clear();
                editTextPassword.getText().clear();
                editTextUserName.requestFocus();*/

            }
        });
        mt.execute();

    }
}
