package activities.com.libdemo;

/**
 * Created by Ahmed on 11/13/2015.
 */
public enum ViewType {
    EditText,
    CheckBox,
    RadioButton,
    Spinner,
    Button,
    View,
    ScrollView,
    ImageView,
}
