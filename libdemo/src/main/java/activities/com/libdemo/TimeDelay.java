package activities.com.libdemo;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Created by Ahmed on 11/12/2015.
 */
public class TimeDelay {
    public int time, audioTime;
    public Uri audio;
    public String message;
    Context mContext;
    Point p;

    public View v;


    public ViewType viewType;
    public TimeComplete timeComplete;


    private EditText editText;
    private CheckBox checkBox;
    private RadioButton radioButton;
    private Spinner spinner;
    private Button button;
    public String popMessage;

    @Deprecated
    public TimeDelay(Context mContext, int time, int audioTime, ViewType viewType, View v, String message, Uri audio, TimeComplete timeComplete) {
        this.mContext = mContext;
        this.timeComplete = timeComplete;
        this.viewType = viewType;
        this.v = v;
        //onWindowFocusChanged(v);
        this.message = message;
        this.audio = audio;
        this.time = time;
        this.audioTime = audioTime;

    }

    public TimeDelay(Context mContext, int time, int audioTime, ViewType viewType, int id, String message, int audio, String popMessage) {
        this.mContext = mContext;
        this.popMessage=popMessage;
        this.viewType = viewType;
        this.v = ((Activity)mContext).findViewById(id);
        this.message = message;
        if(audio!=0) {
            this.audio = Uri.parse("android.resource://" + mContext.getPackageName() + "/" + audio);
        }
        this.time = time;
        this.audioTime = audioTime;

    }

    public void run(TimeComplete timeComplete) {
        this.timeComplete = timeComplete;

        playAudio();

    }

    private void playAudio() {
        if(audio!=null) {
            showPopup((Activity) mContext, popMessage);
            MediaPlayer mPlayer2;
            mPlayer2 = MediaPlayer.create(mContext, audio);
            mPlayer2.start();

            CountDownTimer cd = new CountDownTimer(audioTime, 1000) {
                @Override
                public void onTick(long millisUntilFinished) {

                }

                @Override
                public void onFinish() {
                    startDelay();
                }
            };
            cd.start();
        }else{
            startDelay();
        }

    }

    private PopupWindow pw;
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void showPopup(final Activity context, String popMessage) {
//        int popupWidth = 350;
//        int popupHeight = 150;
        /////////////////////////////
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_layout, null, false);
        int width = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 130, context.getResources().getDisplayMetrics());
        pw = new PopupWindow(layout, width, LinearLayout.LayoutParams.WRAP_CONTENT, true);
        pw.setOutsideTouchable(true);
        pw.setTouchable(true);
        ((TextView)pw.getContentView().findViewById(R.id.poptext)).setText(popMessage);
       pw.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.pop_up_bg_drawable));



        pw.showAsDropDown(this.v ,20,-5);



    }

    ////////////////////////////////////////////////

    private void startDelay() {
        CountDownTimer cd = new CountDownTimer(time, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                timeComplete.onTimeComplete();

                setValues();
                if(pw!=null) {
                    pw.dismiss();
                }
            }
        };
        cd.start();
    }

    private void setValues() {
        switch (viewType) {
            case EditText:
                ((EditText) v).setText(message);
                ((EditText)v).requestFocus();
                break;
            case CheckBox:
                ((CheckBox) v).setChecked(true);
                break;
            case RadioButton:
                ((RadioButton) v).setChecked(true);
                break;
            case Spinner:
                ((Spinner) v).setSelection(2);
                break;
            case Button:
                ((Button) v).performClick();
                break;
            case ScrollView:

                ((ScrollView)v).fullScroll(ScrollView.FOCUS_DOWN);
                break;
            case ImageView:

                ((ImageView)v).performClick();
                break;


        }
    }
}
