package activities.com.libdemo;

import java.util.ArrayList;

/**
 * Created by Ahmed on 11/13/2015.
 */
public class MyTasks {
    ArrayList<TimeDelay> tasks = new ArrayList<>();
    public TimeComplete timeComplete;
    int index = 0;
    boolean isRunning = true;

    public void setOnCompleteTasksListner(TimeComplete timeComplete) {
        this.timeComplete = timeComplete;
    }

    public void add(TimeDelay task) {
        tasks.add(task);
    }

    public void execute() {
        run();
    }
    private  void run(){
            if(index<tasks.size() && isRunning){

                tasks.get(index).run(new TimeComplete() {
                    @Override
                    public void onTimeComplete() {
                        index++;
                        run();
                    }
                });

            }else{
                timeComplete.onTimeComplete();
            }
    }

    public void cancel() {
        isRunning = false;
        tasks.clear();
    }
    public  boolean isRunning(){
        return isRunning;
    }
}
